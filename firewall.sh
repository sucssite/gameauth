#!/bin/bash
### BEGIN INIT INFO
# Provides:          games-firewall
# Required-Start:    $network $syslog $remote_fs
# Required-Stop:     $network $syslog $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Set firewall rules and clear games user table
# Description:       Sets default INPUT policy to DROP and adds default
#                    rules for access from SUCS and for exposed services.
#                    Sets default INPUT policy to accept when stopped.
#                    The game server user table is purged when the firewall
#                    is started or stopped in order to ensure it reflects the
#                    current firewall state.
### END INIT INFO

start(){
	/sbin/iptables -F INPUT
	/sbin/iptables -P INPUT DROP
	/sbin/iptables -A INPUT -i lo -j ACCEPT
	/sbin/iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
	/sbin/iptables -A INPUT -s 137.44.10.0/24 -j ACCEPT
	/sbin/iptables -A INPUT -p TCP --dport 80 -j ACCEPT
	/sbin/iptables -A INPUT -p TCP --dport 443 -j ACCEPT
	/var/www/gameauth/gameauth-task.php
}
stop(){
	/sbin/iptables -F INPUT
	/sbin/iptables -P INPUT ACCEPT
	/var/www/gameauth/gameauth-task.php
}

case "$1" in
	start)
		echo -n "Starting firewall..."
		start
		echo "done."
		;;
	stop)
		echo -n "Stopping firewall..."
		stop
		echo "done."
		;;
	restart)
		echo -n "Restarting firewall..."
		stop
		start
		echo "done."
		;;
	force-reload)
		echo -n "Restarting firewall..."
		stop
		start
		echo "done."
		;;
	*)
		echo "Usage: $0 {start|stop|restart|force-reload}"
		echo "restart and force-reload are equivalent"
		exit 1
esac