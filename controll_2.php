<?php

    /* Functions in this file:
    login($username,$passoword) - checks the credentials aganist sucs & uni ldaps
    authCheck($authd,$username) - checks the username and authd area again ban & allow flags
    renew() - checks the players session and tries to renew if allowed
    logout() - removes the session from the database and clears the cookie

    Login Flow:
        call login passing it the username and password
        This will call the ldapAuth function included from ldap-auth.php
        It when then call authCheck with the info to see what the user can do
        If they are allowed access they are then inserted into the database
    Renew Flow:
        call renew, it has no arguments as it works off session_id
        Firstly it gets the username and which ldap server they authd aganist from the database
        Then it calls authCheck with the retrived data to check what they can still do
        If they are still allowed acess the timeout their database entry is update
        Otherwise they are removed from the database
    Logout Flow:
        call logout, it has no arguments as it works off session_id
        It remvoes the user's database entry
        Then destroys the session logging them out fully
    */

    include('./ldap-auth/ldap-auth.php');
    error_reporting(E_ERROR);

    session_start();

    $DB_PATH = "/opt/gameauth/gameauth.db";
    $DB_CON;
    if (!file_exists($DB_PATH)){
        $DB_CON = new SQLite3($DB_PATH);
        $DB_CON->exec("CREATE TABLE gamers
    		(
        		username TEXT PRIMARY KEY NOT NULL,
        		sessionid TEXT NOT NULL,
        		IP TEXT NOT NULL,
        		authd TEXT NOT NULL,
        		lastseen INT NOT NULL
    		)"
    	);
    	$DB_CON->exec("CREATE TABLE bans
    		(
        		username TEXT PRIMARY KEY NOT NULL,
        		reason TEXT
    		)"
    	);
    } else {
        $DB_CON = new SQLite3($DB_PATH);
    }

    function sqlite3Exists($table,$col,$val){
        global $DB_CON;
        $query = $DB_CON->prepare("SELECT 1 FROM $table WHERE $col = :val LIMIT 1");
        $query->bindValue(':val', $val);
        return (bool) $query->execute()->fetchArray();
    }
    function sqlite3Exec($query){
        global $DB_CON;
        return $DB_CON->query($query);
    }

    //Checks how authed the user is and returns an obejct describing it
    function authCheck($authd,$username){
        //Not a valid user
        if ($authd != "sucs" && $authd != "uni"){
            return [
                level => "NO_LOGIN",
                loginError => "BAD_LOGIN"
            ];
        }

        //Check if they are banned
        if (sqlite3Exists("bans","username",$username)){
            return [
                level => "NO_GAMES",
                loginError => "BANNED"
            ];
        }

        //if they are sucs they are always allowed on
        //or if the uniAllowPath is there (since they will then be uni students)
        if ($authd == "sucs" || file_exists($uniAllowFilePATH)) {
            $accessLevel = "GAME_ACCESS";
        } else {
            //Otherwise they get no games.
            $accessLevel = "NO_GAMES";
            $failReason = "UNI_DISALLOWED";
        }
        return [
            level => $accessLevel,
            loginError => $failReason
        ];
    }

    function login($username,$password){
        //Check to make sure we have a username and password
        if ($username == "" || $password == "") {
                return [
                    level => "NO_LOGIN",
                    loginError => "MISSING_USERNAME_OR_PASSWORD"
                ];
        };

        //Auth the user
        $authd = ldapAuth($username,$password);

        //If they logged in with a email we will detect it and string out username
        if(filter_var($username, FILTER_VALIDATE_EMAIL)){
            //Split the email using "@" as a delimiter
            $s = explode("@",$username);
            //Remove the domain (last element), then recombine it
            array_pop($s);
            $username = implode("@",$s);
        }
        $username = strtolower($username);

        $authResult = authCheck($authd,$username);
        //If they gave a good login
        if($authResult["level"] == "GAME_ACCESS"){
            //Add them into the database
            $sessionid = session_id();
            $cip = $_SERVER['REMOTE_ADDR'];
            $time = time();
            sqlite3Exec("DELETE FROM gamers WHERE username='$username'");
            sqlite3Exec("INSERT INTO gamers (username,sessionid,IP,authd,lastseen) VALUES ('$username','$sessionid','$cip','$authd','$time')");
        }

        //Return the authResult
        return $authResult;
    }

    function renew(){
        $sessionid = session_id();
        if (sqlite3Exists("gamers","sessionid",$sessionid)){
            $query = sqlite3Exec("SELECT authd,username FROM gamers WHERE sessionid='$sessionid';");
            $row = $query->fetchArray(SQLITE3_NUM);
            $authd = $row[0];
            $username = $row[1];

            $authResult = authCheck($authd,$username);

            //Check their login is still good and update if so
            if($authResult["level"] == "GAME_ACCESS"){
                $time = time();
                sqlite3Exec("UPDATE gamers SET lastseen='$time' WHERE sessionid='$sessionid'");
            } else {
                //If it's bad (maybe they have been banned?) delete it and return an error.
                sqlite3Exec("DELETE FROM gamers WHERE sessionid='$sessionid'");
                return $authResult;
            }
            return $authResult;
        } else {
            return [
                level => "NO_LOGIN",
                loginError => "TIMEOUT"
            ];
        }
    }

    function logout(){
        $sessionid = session_id();
        sqlite3Exec("DELETE FROM gamers WHERE sessionid='$sessionid'");
        session_destroy();
        return [
            level => "NO_LOGIN",
            loginError => null
        ];
    }

    function serverData(){
        chdir('games');
        include '_manager.php';
        return getGameStatus();
    }
?>
