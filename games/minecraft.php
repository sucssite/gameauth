<?php namespace minecraft;

function getInfo(){

    $ADDRESS = "games";
    $PORT = 25565;

    if (($sock = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    if(socket_connect($sock,$ADDRESS,$PORT) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    //Documentation on the ping protcol: http://wiki.vg/Server_List_Ping

    //        Size   Protcol Version          Next State
    //         _|_ Id_ _|_ _Address____ _Port__ _|_  _Request Packet
    $input = "\x0f\x00\x2f\x09127.0.0.1\x63\xdd\x01\x01\x00";
    socket_write($sock,$input,strlen($input));

    //Read an inital response from the socket
    //This will let us get an initial size.
    $out = socket_read($sock,8);

    //Get the length of the packet
    $t = readVarint($out);
    $out = $t["data"];
    $len = $t["res"];

    //Keep reading from the socket
    $toRead = $len - strlen($out);
    while($toRead){
      $out .= socket_read($sock,$toRead);
      $toRead = $len - strlen($out);
    }
    socket_close($sock);

    if ( substr($out,0,1) !== "\x00"){
        return ["online"=>false,"error"=>"server sent unexpected result"];
    }


    $t = readVarint(substr($out,1));
    $out = $t["data"];
    $len = $t["res"];

    $result = json_decode($out,$assoc=true);

    if (!is_array($result)){
        return ["online"=>true,"error"=>$result];
    }

    $players = [];

    if ($result["players"]["sample"]){
        foreach ($result["players"]["sample"] as $p){
            $players[] = $p["name"];
        }
    }

    return [
        "_online"=>true,
        "description"=>$result["description"],
        "players_on"=>$result["players"]["online"],
        "players_max"=>$result["players"]["max"],
        "version"=>$result["version"]["name"]
    ];
}

function readVarint($data){
    $result = 0;
    $first = true;
    for($i=0;$i<strlen($data);$i++){
        $part = ord(substr($data,$i,1));
        $result |= ($part & 0x7F) << 7 * $i;
        if (($part & 0x80) == 0){
            break;
        }
    }
    return ["res"=>$result,"data"=>substr($data,$i+1)];
}
?>
