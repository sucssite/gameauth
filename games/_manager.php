<?php
    $GAMES_TO_INCLUDE = [
        "minecraft",
        "sauerbraten"
    ];

    function getGameStatus($templateHeader = FALSE,$templateFooter = FALSE){
        global $GAMES_TO_INCLUDE;

        $gameInfo = [];
        foreach ($GAMES_TO_INCLUDE as $game){
            include "$game.php";
            $a = "$game\\getInfo";
            $thisGame = @$a();
            $gameInfo[$game] = $thisGame;
            if ($templateHeader !== FALSE){
                echo $templateHeader;
                echo "<div data-target=\"$game\">";

                echo preg_replace_callback("/{{([^|]*)\|([^}]*)}}/",function ($matches) use ($thisGame){
                    $elem = $matches[1];
                    $key = $matches[2];
                    $val = $thisGame[$key];
                    if ($val === undefined) {$val="??";}
                    if ($key == "_online"){
                        if($val){
                            $val = "<span style='color:green'>Online</span>";
                        } else {
                            $val = "<span style='color:red'>Offline</span>";
                        }
                    } else if (is_array($val)){
                        $nVal = "";
                        foreach($val as $e){
                            $nVal .= "<li>$e</li>";
                        }
                        $val = $nVal;
                    }
                    return "<$elem data-target='$key'>$val</$elem>";
                },file_get_contents("$game.html"));

                echo "</div>";
                echo $templateFooter;
            }
        }
        return $gameInfo;
    }
?>
