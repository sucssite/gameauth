<?php namespace sauerbraten;

function getInfo(){

    $ADDRESS = "games.sucs.org";
    $PORT = 28786;

    if (($sock = socket_create(AF_INET,SOCK_DGRAM,0)) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    if(socket_connect($sock,$ADDRESS,$PORT) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    $reqId = "GI\n";
    socket_write($sock,$reqId,3);

    $out = socket_read($sock,2048);
    socket_close($sock);

    //Check the response starts with the request Id
    if ( substr($out,0,strlen($reqId)) !== $reqId){
        return ["online"=>false,"error"=>"server sent unexpected response"];
    }
    //Chop off request Id
    $out = substr($out,strlen($reqId));

    //Read players & number of attributes
    $numPlayers = readInt($out);
    $numAttrs = readInt($out);

    //Rad the basic version
    $protcolVersion = readInt($out);
    $gameMode = readInt($out);
    $timeLeft = readInt($out);
    $maxClients = readInt($out);
    $masterMode = readInt($out);

    //Check if we have 7 attributes
    if ($numAttrs == 7){
        //If we do we know if the game is paused and the speed
        $gamePaused = readInt($out);
        $gameSpeed = readInt($out);
    } else {
        $gamePaused = 0;
        $gameSpeed = 100;
    }
    //Everything else is strings, take the reminder and explode on null
    $tmp = explode("\x00",$out);

    //The first part is the map, the second is the desc
    $mapName = $tmp[0];
    $serverDesc = $tmp[1];

    //Conversion from game mode id to string
    $nnn = [
        "Free for all",
        "Coop Edit",
        "Teamplay",
        "Instagib",
        "Instagib Team",
        "Efficiency",
        "Efficiency team",
        "Tactics",
        "Tactics team",
        "Capture",
        "Regen capture",
        "Capture the flag",
        "Insta Capture the flag",
        "Protect",
        "Insta Protect",
        "Hold",
        "Insta Hold",
        "Efficiency Capture the flag",
        "Efficiency Protect",
        "Efficiency Hold",
        "Collect",
        "Insta Collect",
        "Efficiency Collect"
    ];
    $gameMode = $nnn[$gameMode];

    return [
        "_online"=>true,
        "description"=>$serverDesc,
        "map"=>$mapName,
        "players_on"=>$numPlayers,
        "players_max"=>$maxClients,
        "gameMode"=>$gameMode
    ];
}

function readInt(&$out){
    $c = ord(substr($out,0,1));
    if ($c == 128){ //Number that follows is a unsigned sort (2 bytes)
        $c = unpack("v",substr($out,1,2))[1];
        $out = substr($out,3);
    } elseif ($c == -127){ //Number that follows is a unsigned int (4 bytes)
        $c = unpack("V",substr($out,1,4))[1];
        $out = substr($out,3);
    } else { //Number is just a byte
        $out = substr($out,1);
    }
    return $c;
}

#echo json_encode(sauerbraten());
?>
