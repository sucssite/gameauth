#!/usr/bin/php -q
<?php

// this script gets run by root every minute on games

$returnVal;
$db;
$dbPATH = '/opt/gameauth/gameauth.db';
$allow_ipPATH = '/var/www/gameauth/allow_ip.sh';
$deny_ipPATH = '/var/www/gameauth/deny_ip.sh';

$expiredIPs = array();
$newIPs = array();

if (!file_exists($dbPATH)){
	$db = new SQLite3($dbPATH);
	$db->exec("CREATE TABLE gamers
		(
			username TEXT PRIMARY KEY NOT NULL,
			sessionid TEXT NOT NULL,
			IP TEXT NOT NULL,
			authd TEXT NOT NULL,
			lastseen INT NOT NULL
		)"
	);
	$db->exec("CREATE TABLE bans
		(
			username TEXT PRIMARY KEY NOT NULL,
			reason TEXT
		)"
	);
} else {
	$db = new SQLite3($dbPATH);
}

$expiredTime = time();
$expiredTime = $expiredTime - 60;
// get all the rows from the db
$ipQueryResult = $db->query("SELECT lastseen,IP FROM gamers");

// a while loop that sorts all the IPs into 2 1d arrays, ones that should be in
// the firewall allowed rules and ones that shouldn't.
$i = 0;
while($res = $ipQueryResult->fetchArray(SQLITE3_ASSOC)){
    if(!isset($res['lastseen'])) continue;
    if ($res['lastseen'] < $expiredTime){
        $expiredIPs[$i] = $res['IP'];
    }else{
        $newIPs[$i] = $res['IP'];
    }
    $i++;
}

foreach ($expiredIPs as $IP){
	// it has to be done like this becuase iptables is funky
	exec("'$deny_ipPATH' '$IP'", $result, $returnVal);
	while ($returnVal == 0){
		exec("'$deny_ipPATH' '$IP'", $result, $returnVal);
	}
	$db->exec("DELETE FROM gamers WHERE IP='$IP'");
}

foreach ($newIPs as $IP){
	shell_exec("'$allow_ipPATH' '$IP'");
}

?>
