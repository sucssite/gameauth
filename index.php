<!doctype html>
<?php
	include "controll_2.php";

	$username = $_POST["username"];
	$password = $_POST["password"];
	$renew = $_POST["renew"];
	$logout = $_POST["logout"];
	$response;
	if($renew){
		$response = renew();
	} else if ($logout){
		$response = logout();
	} else {
		$response = login($username,$password);
	}

	$level = $response["level"];
	$loginError = $response["loginError"];

	if ($username == null && $password == null){
		$level = "NO_LOGIN";
		$loginError = "FIRST";
	}

	$ERR_MAP = [
	    "BAD_LOGIN" => "You have entered invalid credentials.",
	    "MISSING_USERNAME_OR_PASSWORD" => "Please enter a username and password.",
	    "BANNED" => "Sorry you are banned. For more information contact games@sucs.org",
	    "ERR_UNKNOWN_AUTH_TYPE" => "An unexpected error occoured - Bad Auth Type.",
	    "UNI_DISALLOWED" => "Only SUCS members are currentlly allowed access."
	];
	$errMsg = $ERR_MAP[$loginError];

	function iff($test,$t,$f){
		if ($test){
			print $t;
		} else {
			print $f;
		}
	}
?>

<html lang="en">
	<head>
		<?php if ($level != "NO_LOGIN") { ?>
			<noscript><meta http-equiv="refresh" content="30"></noscript>
		<?php } ?>

		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/ripples.min.css" rel="stylesheet">
		<link href="css/material-wfont.min.css" rel="stylesheet">

		<title>SUCS Games Server</title>

		<style>
			#loginErrorWrap{
				display:<?php iff($errMsg,"block","none"); ?>
			}
			#login{
				display:<?php iff($level == "NO_LOGIN","block","none"); ?>
			}
			#loginMessage{
				display:<?php iff($level == "GAME_ACCESS" || $loginError == "UNI_DISALLOWED","block","none"); ?>
			}
			#logout{
				display:<?php iff($level != "NO_LOGIN","inline-block","none"); ?>
			}
			#signup{
				display:<?php iff($level == "NO_LOGIN" || $loginError == "UNI_DISALLOWED","inline-block","none"); ?>
			}

		</style>

	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-warning">
			<div class="container">
	        	<div class="navbar-header">
					<a class="navbar-brand" rel="home" href="" title="SUCS">
						<img style="height: 40px; margin-top: -5px;" src="res/sucs_logo.png">
	    			</a>
	        	</div>
	      	</div>
	    </nav>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3>SUCS Games Server</h3>
							<p>
								Being a member of SUCS gives you exclusive access to our games server. If you're living on campus, it's the only way to play multiplayer online games - and it's also accessible to members living off campus, too. See below for a list of the games we currently offer.
								<br>
								<br>
								<h4>How do I play?</h4>
								Before you can connect to our games server, you need to log in to our authentication system. Simply use the login box on the right hand side of this page to log in to the system, using your SUCS username and password or your Student Number and password.
								<br>
								<br>
								Steam users are also welcome to join the <a href="http://steamcommunity.com/groups/swanseauniversitycompsoc">SUCS group</a>
								<br>
								<br>
								<h4>Games Currently Available</h4>
								Click each game for more information, as well as links to download any addons or patches you may need.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<!-- LOGIN SECTION -->

							<div id="loginErrorWrap" class="alert alert-danger" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
						  		<span id="loginError">
									<?php print $ERR_MAP[$loginError]; ?>
								</span>
							</div>

							<form method="post" class="form-login" id="login">
								<div class="form-group">
									<input type="text" name="username" placeholder="Username" class="form-control">
								</div>
								<div class="form-group">
									<input type="password" name="password" placeholder="Password" class="form-control">
								</div>
								<button type="submit" class="btn btn-success">Sign in</button>
								Login with your SUCS username or Student Number
							</form>

							<p id="username"></p>
							<p id="loginMessage">
								<?php if ($level == "GAME_ACCESS") { ?>
										You are now logged into the SUCS Game Server system,
										and can connect to any of the servers we have running by simply specifying the hostname/IP address 'games.sucs.org'.
										This page must be left open while you are playing.
										When you close this window, you will no longer have access to the games server,
										and will have to login again if you wish to play some more.
								<?php } else if ($loginError == "UNI_DISALLOWED") { ?>
										Thank you for taking an interest in playing on the SUCS game server.
										Unfortunately the game server is currently only available to SUCS members,
										you can <a href=\"https://sucs.org/join\">sign up</a>
										to SUCS and get 24/7 access to the server
										plus all the other benefits that come with SUCS membership.
								<?php } ?>
							</p>

							<a class="btn btn-warning" href="http://sucs.org/join" id="signup">Signup</a>

							<form method="post" id="logout">
								<input type="hidden" value="logout" name="username">
								<input type="submit" value="Log out" class="btn btn-danger">
							</form>
						</div>
					</div>
				</div>
				<!-- Removing list of online players because it's kinda pointless as
				It only shows those connected to games auth
				<div class="col-md-4" id="onlineListWrap">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="online-users">
								<p>List of people online: </p>
								<ul id="onlineList" data-target="onlineUsers">
								</ul>
							</div>
						</div>
					</div>
				</div>
				-->
			</div>


			<div class="row" data-target="services">
				<?php
					chdir('games');
					include '_manager.php';
					getGameStatus(
                        '<div class="col-md-4"><div class="panel panel-default"><div class="panel-body">',
                        '</div></div></div>'
                    );
				?>
			</div>
		</div>
		<footer>
			<div class="container text-center">
				<p class="text-muted credit"><p>Site designed and maintained by SUCS. Please email games@sucs.org if you have any problems with this service.</p>
			</div>
		</footer>

		<script src="js/jquery.min.js"></script>
		<script src=refresh.js></script>
	</body>
</html>
