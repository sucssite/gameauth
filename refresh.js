var ERR_MAP = {
    "BAD_LOGIN":"You have entered invalid credentials.",
    "MISSING_USERNAME_OR_PASSWORD":"Please enter a username and password.",
    "BANNED":"Sorry you are banned. For more information contact games@sucs.org",
    "ERR_UNKNOWN_AUTH_TYPE":"An unexpected error occoured - Bad Auth Type.",
    "UNI_DISALLOWED":"Only SUCS members are currentlly allowed access."
},
SUCCESS = "You are now logged into the SUCS Game Server system, and can connect to any of the servers we have running by simply specifying the hostname/IP address 'games.sucs.org'. This page must be left open while you are playing. When you close this window, you will no longer have access to the games server, and will have to login again if you wish to play some more.",
SIGNUP_INFO = "Thank you for taking an interest in playing on the SUCS game server. Unfortunately the game server is currently only available to SUCS members, you can <a href=\"https://sucs.org/join\">sign up</a> to SUCS and get 24/7 access to the server plus all the other benefits that come with SUCS membership.";


function loginRefresh(){
    $.post("endpoint.php",{renew:1},onPostResponse);
}
var REFRESH_ID;
function scheduleRefresh(){
    REFRESH_ID = setTimeout(loginRefresh,30*1000);
}
function cancelRefresh(){
    clearTimeout(REFRESH_ID);
}

function populateExtraData(data,domain){
    if(domain === undefined){
        domain = $("body");
    }
    $.each(data,function(key,value){
        var target = domain.find("[data-target='"+key+"']");
        if (target.length === 0){
            console.warn("failed to find target",key,"under",domain);
            return;
        }
        if (key == "_online"){
            if (value){
                target.css("color","green").text("Online");
            } else {
                target.css("color","red").text("Offline");
            }
        } else if (value === null){
            target.empty();
        } else if (value instanceof Array){
            target.empty();
            for(var i=0;i<value.length;i++){
                $("<li>").text(value[i]).appendTo(target);
            }
        } else if (typeof value == "object") {
            populateExtraData(value,target);
        } else {
            target.text(value);
        }
    });
}

function onPostResponse(response){
    console.log(response);

    //When this response comes back it will be 1 of 5 diffrent state we care about
    //DEFAULT|BANNED|UNI-NO|GAME-ACCESS|BAD-CREDENTIALS

    //Populate extra payload data
    populateExtraData(response.extraPayload);

    //if the response is AS_BEFORE nothing changes, just schedle a refresh
    if (response.level == "AS_BEFORE") {
        scheduleRefresh();
        return;
    }

    //Show an error if there is one
    if (response.loginError){
        $("#loginErrorWrap").show();
        $("#loginError").text(ERR_MAP[response.loginError]||response.loginError);
    } else {
        $("#loginErrorWrap").hide();
    }

    //Display username if we have it
    if(response.username){
        $("#username").show().text("Hello "+response.username);
    } else {
        $("#username").hide();
    }

    //Display login details if not logged in
    if (response.level == "NO_LOGIN"){
        $("#login, #signup").show();
        $("#logout").hide();
    } else {
        $("#login, #signup").hide();
        $("#logout").show();
    }

    //Now lets take care of the other messages we end up having to display
    var welcomeMessage;
    if (response.level == "GAME_ACCESS"){
        welcomeMessage = SUCCESS;
    } else if (response.loginError == "UNI_DISALLOWED"){
        welcomeMessage = SIGNUP_INFO;
        $("#signup").show();
    }

    if (welcomeMessage){
        $("#loginMessage").show().html(welcomeMessage);
    } else {
        $("#loginMessage").hide();
    }

    //Finally if they logged in set the refresh up to happen
    if (response.level != "NO_LOGIN"){
        scheduleRefresh();
    } else {
        cancelRefresh();
    }
}
onPostResponse({level:"NO_LOGIN",extraPayload:{}});

$("form").submit(function(event){
    var data={};
    $(this).find("input").each(function(){
        var t = $(this);
        data[t.attr("name")] = t.val();
        if(t.attr("type") == "password"){
            t.val("");
        }
    });
    $.post("endpoint.php",data,onPostResponse);
    return false;
});
