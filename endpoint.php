<?php
    include 'controll_2.php';
    $username = $_POST["username"];
    $password = $_POST["password"];
    $renew = $_POST["renew"];
    $logout = $_POST["logout"];
    $response;
    if($renew){
        $response = renew();
    } else if ($logout){
        $response = logout();
    } else {
        $response = login($username,$password);
    }
    chdir('games');
    include '_manager.php';
    $response["extraPayload"] = getGameStatus();

    header('Content-Type: application/json');
    echo json_encode($response);
?>
